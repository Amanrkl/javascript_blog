---
title: Event Loop
---

JavaScript has a runtime model based on an **event loop**, which is responsible for executing the code, collecting and processing events, and executing queued sub-tasks.

There is only one thread that executes JavaScript code and this is the thread where the event loop is running. The execution of callbacks is done by the event loop. Whenever the call stack is empty, the first entered unit in the callback queue is brought to the call stack by the event loop which was observing both the call stack and callback queue. 

## **Event loop in browser**

{% asset_img Browser.png %}


- **Memory Heap** — this is where the memory allocation happens, all of our object variables are assigned here in a random manner.
- **Call Stack** — this is where your function calls are stored.
- **Web API's** - These API's are provided by browser which provides additional functionality over V8 engine. The functions which uses these API's are pushed to this container which on completion of the response of Web API is popped out of this container.
- **Queues** - The queues are used to compute the asynchronous code response such that it doesn't block engine to execute further.
    - **Macro Task Queue** - This queue executes async functions like DOM events, Ajax calls and setTimeout and has lower priority than Job queue.
    - **Micro Task Queue** - This queue executes async functions which uses promises and has higher precedence over Message Queue.

## **Event loop in Node.js**

{% asset_img Nodejs.png %}


The Node Server consist of following parts:

- **Event Queue** - On completion of the Thread Pool a callback function is issued and sent to the event queue. When call stack is empty the event goes through the event queue and sends callback to the call stack.
- **Thread Pool** - The thread pool is composed of 4 threads which delegates operations that are too heavy for the event loop. I/O operations, Opening and closing connections, setTimeouts are the example of such operations.
- Event loop in Node.js has different phases which has a FIFO queue of callbacks to execute. While each phase is special in its own way, generally, when the event loop enters a given phase, it will perform any operations specific to that phase, then execute callbacks in that phase's queue until the queue has been exhausted or the maximum number of callbacks has been executed. When the queue has been exhausted or the callback limit is reached, the event loop will move to the next phase, and so on.

### **Phase Overview**

- **Timers**: Callbacks scheduled by `setTimeout` and `setInterval` are executed during this phase.
- **Pending callbacks**: I/O callbacks that were previously deferred to the next loop iteration are executed during this phase.
- **Idle, prepare**: This phase is only used internally by Node.js.
- **Poll**: New I/O events are retrieved and I/O callbacks are executed during this phase (except for callbacks scheduled by timers, callbacks scheduled by `setImmediate`, and close callbacks because those are all handled in different phases).
- **Check**: Callbacks scheduled by `setImmediate` are executed during this phase.
- **Close callbacks**: Close callbacks, like when a socket connection is destroyed, are executed during this phase.
