---
title: Pass by Reference
---
Unlike pass by value in JavaScript, pass by reference in JavaScript does not create a new space in the memory, instead, we pass the reference/address of the actual parameter, which means the function can access the original value of the variable. Thus, if we change the value of the variable inside the function, then the original value also gets changed.

It does not create a copy, instead, it works on the original variable, so all the changes made inside the function affect the original variable as well.

```jsx
let obj1 = {
	name: "Aman",
	tech_stack : "Python"
}
let obj2 = obj1;

console.log(obj1)// {name: "Aman", tech_stack : "Python"}
console.log(obj2)// {name: "Aman", tech_stack : "Python"}

obj1.tech_stack = "JavaScript"

console.log(obj1)// {name: "Aman", tech_stack : "JavaScript"}
console.log(obj2)// {name: "Aman", tech_stack : "JavaScript"}

```

In the above example, we have made a variable obj1 and set it equal to an object, then we have set the value of another variable obj2 equal to obj1. As the equal operator identifies that we are dealing with non-primitive data types, so instead of creating a new memory space, it points obj2 to the same memory space that obj1 is pointed to. Thus when we change (mutate) the value of obj1, then the value of obj2 also gets changed since obj2 is also pointing to the same memory space as obj1 does.

## **Pass by Reference in Object (with Function)**

```jsx
let originalObj = {
name: "Fight Club",
rating: 9.0,
genres: "Action, Suspense"
};

function demo(tmpObj) {
  tmpObj.rating = 9.2;
  console.log(tmpObj.rating);
}

console.log(originalObj.rating);// 9.0
demo(originalObj);// 9.2
console.log(originalObj.rating);// 9.2

```

From the above example, we can see that on changing the value of  tmpObj, the value of originalObj also gets changed. The reason the for this is that when we call demo function and pass the object, then originalObj is passed by its reference, so the local parameter tempObj will point to the same object which we defined, i.e., the originalObj. So, in this case, we are not dealing with two independent copies instead, we have variables that are pointing to the same object, so, any changes made to this object will be visible to the other variable.

## **Pass by Reference in an Array (with Function)**

```jsx
let originalArr = ["Apple", "Mango"];

function pushArray(tmpArr) {
  tmpArr.push('Orange')
  console.log(tmpArr);
}

console.log(originalArr);// ["Apple", "Mango"]
pushArray(originalArr);// ["Apple", "Mango", "Orange"]
console.log(originalArr);// ["Apple", Mango", "Orange"]

```

Here, when we are trying to add a new item to the array stored in tempArr, it also affects the originalArr array. This happens because there are no two separate copies of an array, we are dealing only with one array. The variable tempArr references the same array that was initialized in the variable originalArr.

## **When to Use Pass by Reference?**

When we are passing arguments of large size, it is better to use pass-by-reference in JavaScript as no separate copy is made in the called function, so memory is not wasted, and hence the program is more efficient.