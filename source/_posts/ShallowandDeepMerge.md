---
title: Shallow and Deep Merge
---

### The Shallow Merge

**Shallow merge** is a way of merging in Javascript. In shallow merge only the properties owned by the object will be merged, it will not merge the extended properties or methods. It will not check if the **pointed object** is **nested** and contains some **child objects** having varying keys, it will just overwrite them completely.

For example,

```
// initializing the first object
const obj1 = {
    num1: 1,
    num2: {
        subNum2_1 : 21,
        subNum2_2: 22,
    }
}

// initializing the second object
const obj2 = {
    num3 : 3,
    num2 : {
        subNum2_3 : 23,
    }
}

// merging both objects using the spread(...) operator
const mergedObj = {
    ...obj1,
    ...obj2,
}

// printing the merged object
console.log(mergedObj);

```

**Output**

```
num1: 1
num2: { subNum2_3: 23 }
num3: 3

```

Here we can see that we have the key **num2** in both the **obj1** and **obj2**, and when we shallow merged them, the initial value of **num2**, which was an object, got overwritten with another object which was the initialized value of key **num2** in **obj2**.

### The Deep Merge

Opposite to shallow merge, deep merge performs the deep merging, by going inside the **nested objects** (if present). When we perform deep merge, it automatically does not replaces the value if **two keys are identical**, instead, it first tries to look, if the object is nested, and this continues till it reaches the **base level of that key**. And in this process if is finds **two keys are identical** and they do not have any nested children, then it replaces them, else it just concatenates that child key at its appropriate location in the **parent object**.

Lets' understand this with the help of an example.

```
// initializing the first object
const obj1 = {
    num1: 1,
    num2: {
        subNum2_1: 21,
        subNum2_2: 22,
    }
}

// initializing the second object
const obj2 = {
    num3: 3,
    num2: {
        subNum2_3: 23,
    }
}

// merging both objects using the spread(...) operator
const mergedObj = {
    ...obj1,
    ...obj2,
    num2: {
        ...obj1.num2,
        ...obj2.num2,
    },
}

// printing the merged object
console.log(mergedObj);

```

**Output**

```
num1: 1
num2: { subNum2_1: 21, subNum2_2: 22, subNum2_3: 23 }
num3: 3

```

Here in this example, we can see that while deep merging the **obj1** and **obj2**, the complete object was not overwritten directly; instead, it first went into the nested object, tried to look for identical keys, and as there were none at the **base level**, hence it finally concatenates them at an appropriate location into the parent object.