---
title: Error Handling with Promises
---


Before going into error handling with promises, let’s explore why error handling is essential.

When an error occurs in a program, it can cause the program to crash, produce incorrect results, or simply not function as intended. This can be frustrating for developers. By handling errors properly, we can ensure that our programs continue to run smoothly and produce the desired results, even in the face of unexpected events.

The most natural form of error handling for most developers is the synchronous try..catch construct. Unfortunately, it's synchronous-only, so it fails to help in async code patterns:

```jsx
function foo() {
    setTimeout(function () {
        baz.bar();
    }, 100);
}

try {
    foo();
    // later throws global error from baz.bar()
} catch (err) {
    // never gets here
}
```

try..catch would certainly be nice to have, but it doesn't work across async operations unless there's some additional environmental support.

In callbacks, some standards have emerged for patterned error handling, most notably the "error-first callback" style:

```jsx
function foo(cb) {
    setTimeout(function () {
        try {
            var x = baz.bar();
            cb(null, x); // success!
        } catch (err) {
            cb(err);
        }
    }, 100);
}
foo(function (err, val) {
    if (err) {
        console.error(err);
    } else {
        console.log(val);
    }
});
```

This sort of error handling is technically async-capable, but it doesn't compose well at all. Multiple levels of error-first callbacks woven together with these ubiquitous if-statement checks inevitably will lead you to the perils of callback hell.

## **Error handling in Promises**

Promises allow asynchronous code to apply structured error handling. When using promises, you can pass an error handler to the **_then_** method or use a **_catch_** method to process errors. Just like exceptions in regular code, an exception or rejection in asynchronous code will jump to the nearest
error handler.

## **Using then method**

So error handling in Promises with the rejection handler passed to then(..) . Promises don't use the popular "error-first callback" design style, but instead use "split callbacks" style; there's one callback for fulfillment and one for rejection:

```jsx
var p = Promise.reject("Oops");
p.then(
    function fulfilled() {
        // never gets here
    },
    function rejected(err) {
        console.log(err);
    },
);
```

While this pattern of error handling makes fine sense on the surface, the nuances of Promise error handling are often a fair bit more difficult to fully grasp.
Consider:

```jsx
var p = Promise.resolve(42);
p.then(
    function fulfilled(msg) {
        // numbers don't have string functions,
        // so will throw an error
        console.log(msg.toLowerCase());
    },
    function rejected(err) {
        // never gets here
    },
);
```

If the msg.toLowerCase() legitimately throws an error, why doesn't our error handler get notified? It's because that error handler is for the p promise, which has already been fulfilled with value 42. The p promise is immutable, so the only promise that can be notified of the error is the one returned from p.then(..), which in this case we don't capture.
That should paint a clear picture of why error handling with Promises is error-prone. It's far too easy to have errors swallowed, as this is very rarely what you'd intend.

## **Using catch method**

Promise error handling is of "pit of despair" design. By default, it assumes that one wants any error to be swallowed by the Promise state, and if he forgets to observe that state, the error silently dies in obscurity --usually despair.
To avoid losing an error to the silence of a forgotten/discarded Promise, a "best practice" for Promise chains is to always end your chain with a final catch(..), like:

```jsx
var p = Promise.resolve(42);

p.then(function fulfilled(msg) {
    // numbers don't have string functions,
    // so will throw an error
    console.log(msg.toLowerCase());
}).catch(handleErrors);
```

Because we didn't pass a rejection handler to the then(..), the default handler was substituted, which simply propagates the error to the next promise in the chain. As such, both errors that come into p, and errors that come after p in its resolution (like the msg.toLowerCase() one) will filter down to the final handleErrors(..). Hence, we may have as many .then(..) handlers as we want, and then use a single .catch(..) at the end to handle errors in all of them.
